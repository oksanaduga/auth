import pool from "../db.js";

class UserRepository {
  static async createUser({ userName, hashedPassword, role }) {
    console.log({ userName, hashedPassword, role })
    const result = await pool.query('INSERT INTO users (name, password, role) VALUES ($1, $2, $3) RETURNING *', [userName, hashedPassword, role]);

    return result.rows[0];
  }

  static async getUserData(userName) {
    const result = await pool.query('SELECT * FROM users WHERE name = $1', [userName]);

    if (!result.rows.length) {
      return null;
    }

    return result.rows[0];
  }
}

export default UserRepository;
