import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { Forbidden, Unauthorized } from "../utils/Errors.js";
import ErrorsUtils from "../utils/Errors.js";

dotenv.config();

class TokenService {
  static async generateAccessToken(payload) {
    return await jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30m' });
  }

  static async generateRefreshToken(payload) {
    return await jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '15d' });
  }

  static async checkAccess(req, res, next) {
    const authorizationHeader = req.headers.authorization;

    const token = authorizationHeader?.split(' ')?.[1];

    try {
      if (!token) {
        throw new Unauthorized('Неавторизованный пользователь');
        // return next(new Unauthorized('Неавторизованный пользователь'));
      }

      req.user = verifyAccessToken(token);

    } catch (err) {
      return ErrorsUtils.catchError(res, err);
    }
  }

  static async verifyAccessToken(AccessToken) {
    return await jwt.verify(AccessToken, process.env.ACCESS_TOKEN_SECRET)
  }

  static async verifyRefreshToken(refreshToken) {
    return await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
  }
}

export default TokenService;
