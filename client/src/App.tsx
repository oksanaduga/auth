import { AppRouter } from "@/app/providers/router";
import { Nav } from "./components/Nav/Nav";
import cls from '@/app/styles/app.module.scss';
import { useState } from "react";
{/* {
    drag ? (
        <div
            className={cls.drop}
            onDragEnter={(e) => onStart(e)}
            onDragOver={(e) => onStart(e)}
            onDragLeave={(e) => onLeave(e)}
        >
            отпустите файлы чтобы загрузить
        </div>
    ) : (
        <div
            onDragEnter={(e) => onStart(e)}
            onDragOver={(e) => onStart(e)}
            onDragLeave={(e) => onLeave(e)}
        >
            перетащите файлы
        </div>
    )
} */}
// qwe
export const App = () => {
    const [drag, setDrag] = useState(false);
    const [files, setFiles] = useState<File | null>(null);

    const onLeave = (e: React.DragEvent<HTMLFormElement>) => {
        e.preventDefault();
        setDrag(false)
    }

    const onStart = (e: React.DragEvent<HTMLFormElement>) => {
        e.preventDefault();
        setDrag(true)
    }

    const onDrop = (e: React.DragEvent<HTMLFormElement>) => {
        e.preventDefault();
        setDrag(false);
        if (e.dataTransfer.files && e.dataTransfer.files[0]) {
            setFiles(e.dataTransfer.files[0])
        }
    }

    return (
        <>
            <Nav />
            <AppRouter />
            <div className={cls.center}>
                <form
                    className={drag ? cls.drop : ''}
                    onDragEnter={(e) => onStart(e)}
                    onDragOver={(e) => onStart(e)}
                    onDragLeave={(e) => onLeave(e)}
                    onDrop={(e) => onDrop(e)}
                >
                    {
                        drag ? 'отпустите файлы чтобы загрузить' : 'перетащите файлы'
                    }
                </form>
                <div>
                    {files && (
                        <p>file name {files.name}</p>
                    )}
                </div>
            </div>
        </>
    );
}