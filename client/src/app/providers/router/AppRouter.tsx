import { Route, Routes } from "react-router-dom";
import { routesConfig } from "./config";
import type { AppRouteProps } from './types';

export const AppRouter = () => {
    return (
        <Routes>
            {Object.values(routesConfig).map((route: AppRouteProps) => {
                return (
                    <Route key={route.path} path={route.path} element={route.element} />
                )
            })}
        </Routes>
    );
}