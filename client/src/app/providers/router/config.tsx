import { SignIn } from "@/pages/SignIn";
import { AppRoute } from "./const";
import { AppRouteProps } from "./types";
import { SignUp } from "@/pages/SignUp";
import { Main } from "@/pages/Main";
import { Profile } from "@/pages/Profile";

export const routesConfig: Record<AppRoute, AppRouteProps> = {
    [AppRoute.MAIN]: {
        path: '/',
        element: <Main />,
        authOnly: false,
    },
    [AppRoute.SIGN_IN]:  {
        path: 'sign-in',
        element: <SignIn />,
        authOnly: false,
    },
    [AppRoute.SIGN_UP]: {
        path: 'sign-up',
        element: <SignUp />,
        authOnly: false,
    },
    [AppRoute.PROFILE]: {
        path: 'profile',
        element: <Profile />,
        authOnly: true,
    },
    [AppRoute.NOT_FOUND]: {
        path: '*',
        element: <div>not found</div>,
        authOnly: false,
    }
};
