export enum AppRoute {
    MAIN = 'main',
    SIGN_IN = 'sign_in',
    SIGN_UP = 'sign_up',
    NOT_FOUND = 'not_found',
    PROFILE = 'profile',
    // FORBIDDEN = 'forbidden',
}