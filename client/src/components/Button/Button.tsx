import { ButtonHTMLAttributes, ReactNode, memo } from "react";
import cls from './Button.module.scss';
import classNames from "classnames";

export enum ButtonTheme {
    CLEAR = 'clear',
    OUTLINE = 'outline',
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
    children?: ReactNode;
    theme?: ButtonTheme;
    disabled?: boolean;
};

export const Button = memo((props: ButtonProps) => {
    const {
        className,
        children,
        theme = ButtonTheme.OUTLINE,
        disabled,
        ...otherProps
    } = props;

    return (
        <button
            className={classNames(cls.button, {
                [cls[theme]]: theme,
            }, className)}
            disabled={disabled}
            {...otherProps}
        >
            {children}
        </button>
    );
});