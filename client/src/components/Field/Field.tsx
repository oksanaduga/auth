import { InputHTMLAttributes, memo } from 'react';
import cls from './Field.module.scss'
import classNames from 'classnames';
import { FieldValues, UseFormRegister } from 'react-hook-form';
import { defaultValues } from '@/pages/SignIn/SignIn'
import { SignInValues } from '@/pages/SignIn';

interface FieldProps<T extends object> extends InputHTMLAttributes<HTMLInputElement> {
    className?: string;
    error?: boolean;
    register: UseFormRegister<T>;
    name: string;
    helperText?: string;
};


const typedMemo: <T>(c: T) => T = memo;
// export const Select = typedMemo(<T extends string>(props: ISelectProps<T>) => {...} пример

// export const Field = <T extends object>(props: FieldProps<T>) => { было
export const Field = typedMemo(<T extends object>(props: FieldProps<T>) => {
    const {
        className,
        error,
        register,
        name,
        helperText = '',
        ...otherProps
    } = props;

    return (
        <div className={classNames(cls.inputWrapper, className)}>
            <input
                className={classNames(cls.input, { [cls.error]: error })}
                {...register(name)}
                {...otherProps}
            />
            {error && (
                <p className={classNames(cls.errorText)}>
                    {helperText}
                </p>
            )}
        </div>
    );
});