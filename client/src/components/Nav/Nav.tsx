import { Link } from 'react-router-dom';
import cls from './Nav.module.scss'
import classNames from 'classnames';

interface NavProps {
    className?: string;
}

export const Nav = (props: NavProps) => {

    const { className } = props;

    return (
        <nav className={classNames(cls.navWrapper, className)}>
          <Link className={classNames(cls.active)} to="sign-in">Вход</Link>
          <Link to="sign-up">Регистрация</Link>
          <Link to="/">main</Link>
        </nav>
    );
}