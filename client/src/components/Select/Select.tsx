import { Role } from '@/pages/SignUp/types';
import cls from './Select.module.scss'
import classNames from 'classnames';
import { SelectHTMLAttributes, memo, useMemo } from 'react';
import { SelectOption } from './types';

interface SelectProps<T extends string> extends SelectHTMLAttributes<HTMLSelectElement> {
    className?: string;
    options?: SelectOption<T>[];
}

const typedMemo: <T>(c: T) => T = memo;

export const Select = typedMemo(<T extends string>(props: SelectProps<T>) => {
    const {
        className,
        options = [],
        ...otherProps
    } = props;

    const optionsList = useMemo(() => (
        options.map((option) => (
            <option
                value={option.value}
                key={option.value}
            >
                {option.content}
            </option>
        ))
    ), [options]);

    return (
        <select
            className={classNames(cls.select, className)}
            name='select'
            {...otherProps}
        >
            {optionsList}
        </select>
    );
});