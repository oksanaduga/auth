export interface SelectOption<T> {
    value: T;
    content: T;
}