import ReactDOM from "react-dom/client";
import { App } from './App';
import { BrowserRouter } from "react-router-dom";
import '@/app/styles/index.scss';
import { store } from '@/app/store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';

const root = document.getElementById('root');

if (!root) {
    throw new Error(
        'Контейнер не найден, не удалось вмонтировать реакт приложение',
    );
}

ReactDOM.createRoot(root).render(
    <SnackbarProvider>
        <BrowserRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </SnackbarProvider>
)