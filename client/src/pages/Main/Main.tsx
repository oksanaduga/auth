import { Button } from '@/components/Button';
import { useCallback } from 'react';
import { useLogoutMutation } from '@/pages/SignUp';
import inMemoryJWT from '@/shared/services/inMemoryJWT';
import { showErrorMessage } from '@/shared/utils/showErrorMessage';

export const Main = () => {
    const [logout] = useLogoutMutation()

    const handleLogout = useCallback(async() => {
        try {
            const res = await logout(null);
            
            inMemoryJWT.deleteToken();
        } catch (e) {
            showErrorMessage(e);
        }
    }, [])
     
    return (
        <div>
            <h2>Main page</h2>
            
            <div>
                <Button onClick={handleLogout}>Выйти</Button>
            </div>
        </div>
    );
}