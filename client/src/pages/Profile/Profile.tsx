import { Button } from '@/components/Button';
import cls from './Profile.module.scss'
import classNames from 'classnames';
import { useGetDataMutation } from './profileApi';

import { useCallback, useState } from 'react';
import { showErrorMessage } from '@/shared/utils/showErrorMessage';

interface ProfileProps {}
export const Profile = (props: ProfileProps) => {
    const [getSomeData, { isError, isLoading }] = useGetDataMutation();
    const [someData, setSomeData] = useState<string>('');
    
    const handleGetSomeData = useCallback(async() => {
        try {
            const data = await getSomeData(null).unwrap();
            setSomeData(data);
        } catch (e) {
            showErrorMessage(e);
        }
    }, [someData]);

    return (
        <div>
        <Button onClick={handleGetSomeData}>Get some data</Button>

        {isError ? (
            <>Oh no, there was an error</>
        ) : isLoading ? (
            <>Loading...</>
        ) : someData ? (
            <>
            {someData}
            </>
        ) : null}
    </div>
    );
}