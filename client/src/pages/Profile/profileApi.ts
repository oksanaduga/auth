import { rtkApi } from '@/shared/api/rtkApi';
import inMemoryJWT from '@/shared/services/inMemoryJWT';

const profileApi = rtkApi.injectEndpoints({
  endpoints: (build) => ({
    getData: build.mutation<string, null>({
        query() {
            return {
                url: '/resource/protected',
                headers:
                    inMemoryJWT.getToken() ?
                        {
                            authorization: `Bearer ${inMemoryJWT.getToken()}`
                        } : {},
            }
        }
    }),
  }),
})

export const { useGetDataMutation } = profileApi;