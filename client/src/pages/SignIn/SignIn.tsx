import { Button } from "@/components/Button";
import { ButtonTheme } from "@/components/Button/Button";
import { Field } from "@/components/Field";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup"
import { schemaSignIn } from "../helpers";
import { SignInValues } from "./types";
import classNames from "classnames";
import inMemoryJWT from '@/shared/services/inMemoryJWT';

import cls from './SignIn.module.scss';
import { useSignInMutation } from "./signInApi";
import { showErrorMessage } from "@/shared/utils/showErrorMessage";

export const defaultValues: SignInValues = {
    userName: "",
    password: "",
  };

interface SignInProps {}
export const SignIn = (props: SignInProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors, isSubmitting }
      } = useForm({
        defaultValues,
        resolver: yupResolver(schemaSignIn),
      });

      const [signIn] = useSignInMutation();

      return (
        <form
            className={classNames(cls.signInWrapper)}
            onSubmit={handleSubmit(async(data) => {
                try {
                    const { accessToken, accessTokenExpiration } = await signIn(data).unwrap();

                    inMemoryJWT.setToken(accessToken, accessTokenExpiration);
                } catch (e) {
                    showErrorMessage(e);
                }
            })}
        >
            <h2>Войти в аккаунт</h2>
            <Field
                register={register}
                name='userName'
                error={Boolean(errors.userName)}
                helperText={errors.userName?.message}
            />
            <Field
                register={register}
                name='password'
                error={Boolean(errors.password)}
                helperText={errors.password?.message}
            />
            <Button
                theme={ButtonTheme.OUTLINE}
                type="submit"
                disabled={isSubmitting}
            >
                Войти
            </Button>
        </form>
      );
}