import { rtkApi } from '@/shared/api/rtkApi';
import { SignInValues } from './types';

interface SignInApiResponse {
    accessToken: string;
    accessTokenExpiration: string;
};

const signInApi = rtkApi.injectEndpoints({
  endpoints: (build) => ({
    signIn: build.mutation<SignInApiResponse, SignInValues>({
        query(body) {
            return {
                url: `/auth/sign-in`,
                credentials: 'include',
                method: 'POST',
                body,
            }
        }}),
    refresh: build.mutation<SignInApiResponse, null>({
        query() {
            return {
                url: `/auth/refresh`,
                credentials: 'include',
                method: 'POST',
            }
        }})
    }),
})

export const { useSignInMutation, useRefreshMutation } = signInApi;