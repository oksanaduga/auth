import { Button } from "@/components/Button";
import { ButtonTheme } from "@/components/Button/Button";
import { Field } from "@/components/Field";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup"
import { schemaSignUp } from "../helpers";
import classNames from "classnames";
import { SignUpValues, Role, SelectOptions } from "./types";
import inMemoryJWT from '@/shared/services/inMemoryJWT';

import cls from './SignUp.module.scss';
import { Select } from "@/components/Select";
import { useSignUpMutation } from "./signUpApi";
import { showErrorMessage } from '@/shared/utils/showErrorMessage';

const options: SelectOptions[] = [
    {
        value: Role.ADMIN,
        content: 'Администратор',
    },
    {
        value: Role.USER,
        content: 'Пользователь',
    }
];

export const defaultValues: SignUpValues = {
    userName: "",
    password: "",
    role: Role.USER,
};

export const SignUp = () => {
    const {
        register,
        handleSubmit,
        control,
        formState: { errors, isSubmitting }
      } = useForm({
        defaultValues,
        resolver: yupResolver(schemaSignUp),
      });

      const [signUp] = useSignUpMutation();

      return (
        <form
            className={classNames(cls.signUpWrapper)}
            onSubmit={handleSubmit(async(data) => {
                try {
                    const { accessToken, accessTokenExpiration } = await signUp(data).unwrap();

                    inMemoryJWT.setToken(accessToken, accessTokenExpiration);
                } catch (e) {
                    showErrorMessage(e);
                }
            })}
        >
            <h2>Создать аккаунт</h2>
            <Field
                register={register}
                name='userName'
                error={Boolean(errors.userName)}
                helperText={errors.userName?.message}
            />
            <Field
                register={register}
                name='password'
                error={Boolean(errors.password)}
                helperText={errors.password?.message}
            />
             <Controller
                control={control}
                name='role'
                render={({ field: { onChange, value } }) => (
                    <Select
                        options={options}
                        value={value}
                        onChange={onChange}
                    />
                )}
            />
            <Button
                theme={ButtonTheme.OUTLINE}
                type="submit"
                disabled={isSubmitting}
            >
                Зарегистрироваться
            </Button>
        </form>
      );
}
