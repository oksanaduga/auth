import { rtkApi } from '@/shared/api/rtkApi';
import { SignUpValues } from './types';

interface SignUpApiResponse {
    accessToken: string;
    accessTokenExpiration: string;
};

const signUpApi = rtkApi.injectEndpoints({
  endpoints: (build) => ({
    signUp: build.mutation<SignUpApiResponse, SignUpValues>({
        query(body) {
            return {
                url: `/auth/sign-up`,
                credentials: 'include',
                method: 'POST',
                body,
            }
        }
    }),
    logout: build.mutation<string, null>({
        query() {
            return {
                url: `/auth/logout`,
                credentials: 'include',
                method: 'POST',
            }
        }
    })
  }),
})

export const { useSignUpMutation, useLogoutMutation } = signUpApi;