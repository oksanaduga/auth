export enum Role {
    ADMIN = 'admin',
    USER = 'user',
}

export type SignUpValues = {
    userName: string,
    password: string,
    role: Role,
};

export type SelectOptions = {
    value: Role,
    content: string,
}