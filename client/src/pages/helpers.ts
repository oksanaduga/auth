import * as yup from "yup";
import { Role } from "./SignUp/types";

export const schemaSignIn = yup
  .object()
  .shape({
    userName: yup.string()
      .trim()
      .required("Поле обязательно!")
      .min(3, 'Имя слишком короткое - минимум 3 символа')
      .max(50, "Максимальная длина - 50 символов"),
    password: yup.string()
      .trim()
      .required("Поле обязательно!")
      .min(3, 'Пароль слишком короткий - минимум 3 символа')
      .max(50, "Максимальная длина - 50 символов"),
  })
  .required();

export const schemaSignUp = yup
.object()
.shape({
  userName: yup.string()
    .trim()
    .required("Поле обязательно!")
    .min(3, 'Имя слишком короткое - минимум 3 символа')
    .max(50, "Максимальная длина - 50 символов"),
  password: yup.string()
    .trim()
    .required("Поле обязательно!")
    .min(3, 'Пароль слишком короткий - минимум 3 символа')
    .max(50, "Максимальная длина - 50 символов"),
  role: yup.mixed<Role>().oneOf(Object.values(Role)).required('Пожалуйста выберете роль'),
})
.required();