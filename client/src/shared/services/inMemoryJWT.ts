import { useRefreshMutation } from "@/pages/SignIn/signInApi";

const inMemoryJWTService = () => {
    let token: string | null = null;

    let refreshTriggerId: number | null = null;

    const refreshToken = (expiration: string) => {
        const [refresh] = useRefreshMutation();
        const refreshTrigger = +expiration - 10000;

        console.log('refreshTrigger', refreshTrigger)
        
        refreshTriggerId = setTimeout(async() => {
            try {
                const { accessToken, accessTokenExpiration } = await refresh(null).unwrap();
                console.log('refresh', { accessToken, accessTokenExpiration })

                setToken(accessToken, accessTokenExpiration);
            } catch (e) {
                console.log(e);
            }
        }, refreshTrigger);
    }

    const abortRefreshToken = () => {
        if (refreshTriggerId) {
            clearInterval(refreshTriggerId);
        } 
    }

    const getToken = () => {
        return token;
    }

    const setToken = (newToken: string, expiration: string) => {
        token = newToken;
        refreshToken(expiration);
    }

    const deleteToken = () => {
        token = null;
        abortRefreshToken();
    }

    return { getToken, setToken, deleteToken }
}

export default inMemoryJWTService();