import { enqueueSnackbar } from "notistack";

export const showErrorMessage =  (error) => {
  return enqueueSnackbar(error.data.error, { variant: "error" });
}
